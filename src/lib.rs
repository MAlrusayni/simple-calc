use khalas::{css::unit::px, prelude::*, theme::ant::Ant};

pub struct MyApp {
    number: SpinEntry<Msg>,
    multiple: SpinEntry<Msg>,
    percent: SpinEntry<Msg>,
    calc_btn: Button<Msg>,
    result: f64,
    theme: Ant,
}

impl Default for MyApp {
    fn default() -> Self {
        let number = SpinEntry::new(Msg::Number).set_min(1.).set_step(500.);

        let multiple = SpinEntry::new(Msg::Multiple).set_min(1.).set_step(1.);

        let percent = SpinEntry::new(Msg::Percent).set_min(0.01).set_step(0.01);

        let calc_btn =
            Button::with_label(Msg::CalcBtn, "أحسب").and_events(|conf| conf.click(|_| Msg::Calc));

        Self {
            theme: Ant::default(),
            number,
            multiple,
            percent,
            calc_btn,
            result: 0.0,
        }
    }
}

pub enum Msg {
    Number(spin_entry::Msg),
    Multiple(spin_entry::Msg),
    Percent(spin_entry::Msg),
    CalcBtn(button::Msg),
    Calc,
}

impl Model<Msg, ()> for MyApp {
    type Message = Msg;

    fn update(&mut self, msg: Msg, orders: &mut impl Orders<Msg, ()>) {
        match msg {
            Msg::Number(msg) => self.number.update(msg, orders),
            Msg::Multiple(msg) => self.multiple.update(msg, orders),
            Msg::Percent(msg) => self.percent.update(msg, orders),
            Msg::CalcBtn(msg) => self.calc_btn.update(msg, orders),
            Msg::Calc => match (
                self.number.get_value(),
                self.multiple.get_value(),
                self.percent.get_value(),
            ) {
                (Some(number), Some(multiple), Some(percent)) => {
                    let mut number = number;
                    for _ in 0..multiple as u64 {
                        number *= 1. + percent;
                    }
                    self.result = number;
                }
                _ => {}
            },
        }
    }
}

impl Render<Msg> for MyApp {
    type View = Node<Msg>;
    type Style = ();

    fn style(&self, _: &impl Theme) -> Self::Style {
        ()
    }

    fn render_with_style(&self, theme: &impl Theme, _: Self::Style) -> Self::View {
        let number = Flexbox::new()
            .set_gap(px(4))
            .add(Label::new("المبلغ").render(theme))
            .add(self.number.render(theme))
            .render(theme);

        let multiple = Flexbox::new()
            .set_gap(px(4))
            .add(Label::new("عدد الأيام").render(theme))
            .add(self.multiple.render(theme))
            .render(theme);

        let percent = Flexbox::new()
            .set_gap(px(4))
            .add(Label::new("نسبة الربح").render(theme))
            .add(self.percent.render(theme))
            .render(theme);

        let buttons = Flexbox::new()
            .add(self.calc_btn.render(theme))
            .render(theme);

        Flexbox::new()
            .center()
            .column()
            .full_size()
            .set_gap(px(4))
            .add(number)
            .add(multiple)
            .add(percent)
            .add(buttons)
            .add(Label::new(self.result.to_string()).render(theme))
            .render(theme)
    }
}

#[wasm_bindgen(start)]
pub fn render() {
    App::builder(
        |msg, model: &mut MyApp, orders| {
            model.update(msg, orders);
        },
        |model| model.render(&model.theme),
    )
    .build_and_start();
}
